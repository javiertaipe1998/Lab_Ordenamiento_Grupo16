#include <iostream>
#include <stdlib.h>


using namespace std;

struct nodo{

    int dato;
    struct nodo *sgte;
};
typedef struct nodo *Tlista;
void Insertar_final(Tlista &lista, int valor) {
    Tlista nuevo=NULL, p=lista;
    nuevo = new(struct nodo);
    nuevo ->dato= valor;
    nuevo->sgte=NULL;
    if(p == NULL) {
    	lista = nuevo;
        return;
    }
    while(p!=NULL) {
        if(p->sgte != NULL)
            p = p->sgte;
        else
            break;
    }
    p->sgte = nuevo;
}

void Burbuja (Tlista &lista){
Tlista p= lista;
Tlista aux2=NULL;
int aux;


while(lista->sgte!=aux2){

    while(p->sgte!=aux2){

    if (p->dato > p->sgte->dato){
            aux=p->dato;
            p->dato = p->sgte->dato;
            p->sgte->dato = aux;
            }
     p=p->sgte;
}
aux2=p;
p=lista;
}
}

void Seleccion(Tlista &lista){
	int aux1 = 0;
	Tlista p=lista;
	Tlista q=lista;
	Tlista aux=lista;
	while (p->sgte!=NULL){
		int	menor=10000;
		q = p->sgte;
		aux = p;
		if (q->sgte!=NULL){
			while (q->sgte!=NULL)
			{
				if (menor>q->dato){
					menor=q->dato;
            }
				q=q->sgte;
			}
			if (menor>q->dato){
				menor=q->dato;
			}
		}
		else{
			if (menor>p->dato)
			{
				menor=p->dato;
			}
			if (menor>q->dato)
			{
				menor=q->dato;
			}
		}
		while (aux->dato != menor){
			aux = aux->sgte;
		}
		if (menor<p->dato){
			aux1 = p->dato;
			p->dato = menor;
			aux->dato = aux1;
		}
		p = p->sgte;
	}
}

void intercambio(Tlista &lista){
    int aux;
    Tlista q,p=lista;

    while(p->sgte!=NULL)
    {
        q=p->sgte;
        while(q!=NULL)
        {
            if(q->dato<p->dato)
            {
                aux=q->dato;
                q->dato=p->dato;
                p->dato=aux;
            }
            q=q->sgte;
        }
        p=p->sgte;
    }

}

void insercion(Tlista &lista,int valor){
	int inicio, fin;
	Tlista nuevo, p, q;

	p=lista;
	q=NULL;

	nuevo = new (struct nodo);
	nuevo->sgte=NULL;


	nuevo->dato=valor;

	if(lista==NULL){
		lista = nuevo;
	}else{
		if(lista->sgte==NULL){
			if(nuevo->dato<lista->dato){
				nuevo->sgte=lista;
				lista = nuevo;
			}else{
				lista->sgte=nuevo;
			}
		}else{
			inicio = lista->dato;
			while(p->sgte!=NULL){
				p=p->sgte;
			}
			fin=p->dato;
			p=lista;

			if(nuevo->dato<=lista->dato){
				nuevo->sgte=lista;
				lista=nuevo;
			}else if(nuevo->dato>=fin){
				while(p->sgte!=NULL){
				p=p->sgte;
				}
				p->sgte=nuevo;
			}else{
				while(p->dato<nuevo->dato){
					q=p;
					p=p->sgte;
				}
				q->sgte=nuevo;
				nuevo->sgte=p;
			}
		}
	}
}

void ordenarQuick(Tlista &lista,Tlista aux){




    if(lista!=aux){



    Tlista limd,limi=lista;


    int aux2;
    bool interruptor=true;
    Tlista p=limi;

    int pivot = limi->dato;

    if(limd==limi)
    {

        interruptor=false;
    }
    else {
        interruptor=true;
    }

    do{

    while(interruptor && limi!=limd){

        while(p!=aux){
            limd=p;
            p=p->sgte;
        }

        if(pivot>limd->dato){

            limi->dato=limd->dato;
            limd->dato=pivot;

            interruptor=false;

        }
        aux=limd;
        p=limi;
    }
    while(limi!=limd){
            limi=limi->sgte;
        if(pivot<limi->dato)
        {
            limd->dato=limi->dato;
            limi->dato=pivot;
            interruptor=true;
            break;
        }
    }
}while(limi!=limd);

    ordenarQuick(lista,limi);
    ordenarQuick(limd->sgte,NULL);

}



}
void ordenarShell(Tlista &lista,int tam){



    Tlista q,p= lista;
    Tlista inicio=lista;
    int i=0;
    int aux=tam+2;
    int newon;


    do{
        aux=aux/2;
        while(i<aux){
                p= p->sgte;
                i++;
                q=p;
        }
        while(q != NULL){
                if (inicio->dato > q->dato){
                        newon=q->dato ;
                        q->dato=inicio->dato;
                        inicio->dato=newon;

                }
                    q=q->sgte;
            inicio= inicio->sgte;
        }
        i=0;
        p=lista;
        inicio=lista;

    }while(aux!=0);


}
void separar(Tlista lista,Tlista *inicio,Tlista *ulti){

    Tlista aux;
    Tlista aux1;
        if (lista==NULL || lista->sgte==NULL)
            {
            *inicio = lista;
            *ulti = NULL;
            }
        else{
        aux1 = lista;
        aux = lista->sgte;
        while(aux != NULL){

            aux=aux->sgte;

            if(aux!=NULL){

                aux1=aux1->sgte;
                aux=aux->sgte;
            }
        }
        *inicio = lista;
        *ulti = aux1->sgte;
        aux1->sgte = NULL;
    }
}

Tlista posicionar(Tlista x, Tlista y){
    Tlista aux= NULL;

    if (x == NULL){
        return y;
    }else if (y == NULL){
        return x;
    }

    if (x->dato <= y->dato){
        aux= x;
        aux->sgte = posicionar(x->sgte, y);
    }else{
        aux= y;
        aux->sgte = posicionar(x, y->sgte);
    }
    return aux;

}
void ordenarMerge(Tlista &lista){
    Tlista cab = lista;
    Tlista limi = NULL;
    Tlista limd = NULL;

    if(cab == NULL || cab->sgte == NULL){
        return;
    }
    separar(cab, &limi, &limd);
    ordenarMerge(limi);
    ordenarMerge(limd);

    lista = posicionar(limi, limd);

}
void mostrar(Tlista &lista){
    Tlista p=lista;
    while(p != NULL){
        cout<<p->dato<<" | ";
        p=p->sgte;
    }
  	cout<<endl;
}

int numDeElementos(Tlista &lista){

	Tlista p=lista;
	int n=0;
	while(p!=NULL){
		n++;
		p=p->sgte;
	}
	return n;
}

void menu(){


  cout<<endl;
    cout << "\t\t\t浜様様様様様様様様様様様様様様様様様融" << endl;
	cout << "\t\t\t�                MENU                �" << endl;
	cout << "\t\t\t麺様様様様様様様様様様様様様様様様様郵" << endl;
	cout << "\t\t\t�  1. Agregar por el final           �" << endl;
	cout << "\t\t\t�  2. Ordenamiento por Burbuja       �" << endl;
    cout << "\t\t\t�  3. Ordenamiento por Seleccion     �" << endl;
	cout << "\t\t\t�  4. Ordenamiento por Intercambio   �" << endl;
	cout << "\t\t\t�  5. Ordenamiento por Insercion     �" << endl;
	cout << "\t\t\t�  6. Ordenamiento Shell             �" << endl;
    cout << "\t\t\t�  7. Ordenamiento Quick sort        �" << endl;
    cout << "\t\t\t�  8. Ordenamiento Merge sort        �" << endl;
	cout << "\t\t\t�  9. Mostrar elementos              �" << endl;
	cout << "\t\t\t� 10. Vaciar lista                   �" << endl;
	cout << "\t\t\t� 11. Salir                          �" << endl;
	cout << "\t\t\t藩様様様様様様様様様様様様様様様様様夕" << endl;
	cout << "\t\t\t\tIngrese una opcion: ";
}
int main()
{   int valor;
char rpta;
    int op=0;
    Tlista lista=NULL ;
    int cont = 0;
    int x;
    Tlista p=lista;
    Tlista lista2=NULL;
    do
    {
         menu();
    cin>>op;
     system("cls");

        switch(op) {
        case 1:
           cout<<"\nIngrese un valor: ";
            cin>>x;
            Insertar_final(lista,x);
            cout<<endl;
            mostrar(lista);
            break;
        case 2:
            Burbuja(lista);
            mostrar(lista);
            break;

        case 3:
            Seleccion(lista);
            mostrar(lista);
            break;

        case 4:
            intercambio(lista);
            mostrar(lista);
            break;

        case 5:
            do{
                            cout<<"Ingrese un numero: ";
                            cin>>valor;
						insercion(lista,valor);
						cout<<"\nLista: ";
						mostrar(lista);
						cout<<"\n\nDesea seguir insertando numeros? (s/n): ";
						cin>>rpta;
					}while(rpta=='s');
					break;

        case 6:
            cout<<"MOSTRANDO LISTA ORDENADA"<<endl;
            cont=numDeElementos(lista);
            ordenarShell(lista,cont);
            mostrar(lista);
            break;
        case 7:
            ordenarQuick(lista,NULL);
            mostrar(lista);

            break;
        case 8:
            ordenarMerge(lista);
            mostrar(lista);
            break;
        case 9:
        	cout<<"MOSTRANDO LISTA"<<endl;
            mostrar(lista);
            break;
      	case 10:
		  	lista=NULL;
			break;

        case 11:
		  	system("exit");
			break;

        default:
            cout<<"Opcion Incorrecta"<<endl;
        }

    }while(op!=11);
}
