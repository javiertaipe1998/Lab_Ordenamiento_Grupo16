#include <iostream>
using namespace std;

struct nodo{
    char dato;
    struct nodo *sgte;
};
typedef struct nodo *Tlista;


void Push(Tlista &lista, char producto) {
    Tlista nuevo=NULL, p=lista;
    nuevo = new(struct nodo);
    nuevo ->dato= producto;
    nuevo->sgte=NULL;
    if(p == NULL) {
    	lista = nuevo;
        return;
    }
    while(p!=NULL) {
        if(p->sgte != NULL)
            p = p->sgte;
        else
            break;
    }
    p->sgte = nuevo;

}
void Pop (Tlista &lista,Tlista &destino){
    Tlista p= lista;
    if ( lista == NULL){
        cout<<"Error, no hay productos";
    }
    else{
        lista = p->sgte;
        Push(destino,p->dato);
    }
}

void PopEliminar (Tlista &lista){
    Tlista p= lista;
    if ( lista == NULL){
        cout<<"Error, no hay productos";
    }
    else{
        lista = p->sgte;
        delete(p);

    }
}


void mostrar(Tlista &lista){
    Tlista p=lista;
    while(p != NULL){
        cout<<p->dato<<" | ";
        p=p->sgte;
    }
  	cout<<endl;
}

int main()
{   char producto,rpta,letra;
    Tlista lista= NULL;
    Tlista p;
    Tlista destino= NULL;
    rpta = 's';
    while(rpta == 's'){
            cout << "Ingrese el producto" << endl;
            cin>>producto;
            Push(lista,producto);
            mostrar(lista);           
            cout<<"Desea agregar mas?"; cin>>rpta;
            if(rpta != 's'){
            	
            	Pop(lista,destino);
		  		  PopEliminar(lista);
				Pop(lista,destino);
				Pop(lista,destino);
                    	mostrar(destino);
            }

    }

    return 0;
}

